package net.weamor.interfaces.run;

import java.io.File;
import java.io.FileNotFoundException;

import net.weamor.interfaces.ServiceConfig;
import net.weamor.interfaces.utils.xml.Dom4jParserXml;
import net.weamor.interfaces.utils.xml.XmlDocument;

import org.dom4j.DocumentException;

import com.jfinal.config.JFinalConfig;

/** 
 * @ClassName InterfaceConfig  
 * @Description 配置文件基类  其他的config类继承此类
 *   
 */
public abstract class InterfaceConfig extends JFinalConfig{
	
	/**
	 * 初始化配置文件
	 * @Title: configFile 
	 * @param configFile
	 * @throws FileNotFoundException
	 * @throws DocumentException 参数说明
	 * @return void    返回类型
	 * @throws
	 */
	public void configFile(String configFile) throws FileNotFoundException, DocumentException{
		XmlDocument xmldoc = new Dom4jParserXml();
		File file = new File(configFile);
		
		if (file.exists()) {
			ServiceConfig.configMap = xmldoc.parserXmlByMutilNode(configFile);
		} else {
			throw new FileNotFoundException("#E " + configFile + " 配置文件找不到,请修改调用configFile的类文件路径");
		}
	}

}
