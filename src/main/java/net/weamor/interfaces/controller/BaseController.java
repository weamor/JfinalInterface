/**   
 * @Project: JfinalInterface 
 * @Title: BaseController.java 
 * @Package net.weamor.interfaces.controller 
 * @Description: 基础的Controller类 其他的controller 应该要继承此类 
 * @author weamor
 * @date 2015年9月14日 下午8:59:38 
 * @version V1.0   
 */
package net.weamor.interfaces.controller;

import java.util.Map;

import net.weamor.interfaces.ServiceConfig;
import net.weamor.interfaces.pojo.RequestObject;
import net.weamor.interfaces.pojo.RequestParam;
import net.weamor.interfaces.pojo.config.ThirdBean;
import net.weamor.interfaces.service.RequestService;

import net.weamor.interfaces.utils.MyCaptchaRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.core.Controller;

/** 
 * @ClassName BaseController  
 * @Description 基础的Controller类 其他的controller 应该要继承此类
 * @author weamor
 * @date 2015年9月14日  
 *   
 */
public abstract class BaseController extends Controller{

	private static Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	/**
	 * 处理url和参数 
	 * @Title: dealWithRequestParam 
	 * @Description: 处理url和参数  
	 * @param getUrl			是否需要获取配置文件的url
	 * @param getParam			是否需要获取配置文件的param
	 * @return 参数说明
	 * @return RequestParam    返回类型
	 */
	public RequestParam dealWithRequestParam(boolean getUrl,boolean getParam){
		RequestObject requestObject = getAttr("requestParam");
		//根据pro和method拿到指定的配置文件信息
		ThirdBean thirdBean = ServiceConfig.configMap.get(requestObject.getPro());
		
		Map<String, Object> argMap = null;
		String url = "";
		String protocol = "";
		if (thirdBean != null) {
			protocol = thirdBean.getConfigParam().get("protocol");
			if (getUrl) {
				//获取配置请求配置信息 
				url = RequestService.dealWithUrl(requestObject);
			}
		}

		//当要获得参数为true的时候，才重新读取并且处理参数
		if (getParam){
			try {
				argMap = RequestService.dealWithParams(requestObject);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("#E 请求参数为:url"+url + " argMap:"+argMap.toString() +" 发生了异常"+e.getMessage());
			}
		}
		
		RequestParam requestParam = packRequestParam(argMap, url, protocol, requestObject, thirdBean);
		return requestParam;
	}
	
	
	public RequestParam dealWithRequestParam(){
		RequestObject requestObject = getAttr("requestParam");
		//根据pro和method拿到指定的配置文件信息
		ThirdBean thirdBean = ServiceConfig.configMap.get(requestObject.getPro());
		
		Map<String, Object> argMap = null;
		String url = "";
		String protocol = "";
		if (thirdBean != null) {
			protocol = thirdBean.getConfigParam().get("protocol");
			
			//获取配置请求配置信息 
			url = RequestService.dealWithUrl(requestObject);
		}
		
		try {
			argMap = RequestService.dealWithParams(requestObject);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("#E 请求参数为:url"+url + " argMap:"+argMap.toString() +" 发生了异常"+e.toString());
		}
		
		RequestParam requestParam = packRequestParam(argMap, url, protocol, requestObject, thirdBean);
		
		return requestParam;
	}

	/**
	 * 获得一个图形验证码
	 */
	public void getCaptcha(){
		render(new MyCaptchaRender(60,22,4,true));
	}

	/**
	 * 验证图形验证码
	 * @param captchName		前端定义的图形验证码name
	 * @return
	 */
	public boolean validateCaptch(String captchName){

		/*
		具体实现为：
		String inputRandomCode = getPara("captcha");
		//执行验证
        boolean validate = MyCaptchaRender.validate(this, inputRandomCode);
		 */
		String inputRandomCode = getPara(captchName);
		boolean validate = MyCaptchaRender.validate(this, inputRandomCode);
		return  validate;
	}
	
	private RequestParam packRequestParam(Map<String, Object> argMap,String url,String protocol,
			RequestObject requestObject,ThirdBean thirdBean){
		
		//对于客户端，在此封装
		RequestParam requestParam = new RequestParam();
		requestParam.setArgMap(argMap);
		requestParam.setUrl(url);
		requestParam.setParam(requestObject.getParam());
		requestParam.setMethod(requestObject.getMethod());
		requestParam.setProtocol(protocol);
		requestParam.setPro(requestObject.getPro());
		requestParam.setCaller(requestObject.getCaller());
		requestParam.setParamstyle(requestObject.getParamstyle());
		requestParam.setSparam(requestObject.getSparam());
		
		//将配置文件配置的第三方信息返回
		requestParam.setThirdBean(thirdBean);
		
		return requestParam;
	}
	
}
