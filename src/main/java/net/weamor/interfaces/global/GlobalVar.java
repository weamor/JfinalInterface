package net.weamor.interfaces.global;

import java.io.Serializable;

/**
 * 各种返回编码值及其对应含义
 */
public class GlobalVar implements Serializable {
	
    private static final long serialVersionUID = 7339541141888044932L;

    public static final String DEFAULT_CHARSET = "UTF-8";
	
	//默认项目名称 用于记录mongodb以及项目名称等
	public final static String PRO_NAME = "unify";
	
	/**返回代码**/
	public final static String CODE = "code";
	
	/**描述错误返回信息**/
	public final static String MSG = "msg";
	
	/**数据集**/
	public final static String DATA = "data";
	
	/**保留字段*/
	public final static String RETAIN = "retain";


	/**
	 * 参数处理方式定义
	 */
	public final static String PARAM_MD5 = "MD5";
	public final static String PARAM_DEFAULT = "DEF";
	public final static String PARAM_TIME = "TIME";
	public final static String PARAM_URLENCODER = "URLENCODER";

	/**
	 * 返回值加密方式
	 */
	public static final String ENCRY = "BASE64";

	/**
	 * 请求方式
	 */
	public static final String HTTP_POST = "POST";
	public static final String HTTP_TCP = "TCP";
	public static final String HTTP_GET = "GET";

	/**
	 * 参数样式
	 */
	public static final String PARAMSTYLE_JSON = "JSON";
	public static final String PARAMSTYLE_XML = "XML";
	public static final String PARAMSTYLE_OTHER = "OTHER";

	/**
	 * 调用方法
	 */
	public static final String INVOKE_REFLECT = "REFLECT";
	public static final String INVOKE_HTTP = "http";
	public static final String HTTPS = "https";

	/**
	 * HTTP状态码标识
	 */
	public static final int HTTP_404 = 404;
	public static final int HTTP_500 = 500;
	public static final String HTTP_200 = "200";


	/**
	 * 操作成功
	 */
	public static final String RC_SUCCESS = "0000";
	
	//操作失败
	public static final String RC_FAIL = "-3003";

	public static final String RM_SUCCESS = "操作成功";
	public static final String RM_FAIL = "操作失败";
	
	
	// 数据接收或请求类型
	public static enum VALUE_TYPE {
		REQ,	//请求
		RESP	//响应
	}
	
	
}
