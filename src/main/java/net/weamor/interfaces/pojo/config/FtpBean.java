package net.weamor.interfaces.pojo.config;

/** 
 * @ClassName Ftp  
 * @Description FTP对象 
 *   
 */
public class FtpBean {

	private String ftpHost;
	private String ftpUserName;
	private String ftpUserPasswd;
	private int ftpPort;
	private String ftpKey;
	// ftp类型 sftp | ftp
	private String ftpType;

	public String getFtpType() {
		return ftpType;
	}

	public void setFtpType(String ftpType) {
		this.ftpType = ftpType;
	}

	/** 
	 * @return ftpHost 
	 */

	public String getFtpHost() {
		return ftpHost;
	}

	/** 
	 * @param ftpHost 要设置的 ftpHost 
	 */

	public void setFtpHost(String ftpHost) {
		this.ftpHost = ftpHost;
	}

	/** 
	 * @return ftpUserName 
	 */

	public String getFtpUserName() {
		return ftpUserName;
	}

	/** 
	 * @param ftpUserName 要设置的 ftpUserName 
	 */

	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}

	/** 
	 * @return ftpUserPasswd 
	 */

	public String getFtpUserPasswd() {
		return ftpUserPasswd;
	}

	/** 
	 * @param ftpUserPasswd 要设置的 ftpUserPasswd 
	 */

	public void setFtpUserPasswd(String ftpUserPasswd) {
		this.ftpUserPasswd = ftpUserPasswd;
	}

	/** 
	 * @return ftpPort 
	 */

	public int getFtpPort() {
		return ftpPort;
	}

	/** 
	 * @param ftpPort 要设置的 ftpPort 
	 */

	public void setFtpPort(int ftpPort) {
		this.ftpPort = ftpPort;
	}

	/** 
	 * @return ftpKey 
	 */

	public String getFtpKey() {
		return ftpKey;
	}

	/** 
	 * @param ftpKey 要设置的 ftpKey 
	 */

	public void setFtpKey(String ftpKey) {
		this.ftpKey = ftpKey;
	}

}
