package net.weamor.interfaces.pojo.config;

/** 
 * @ClassName Host  
 * 匹配配置文件的每一个interface中的hosts 
 */
public class HostBean {
	private String id;
	private String url;
	private String key;
	private String remark;
	
	/** 
	 * @return id 
	 */
	
	public String getId() {
		return id;
	}
	/** 
	 * @param id 要设置的 id 
	 */
	
	public void setId(String id) {
		this.id = id;
	}
	/** 
	 * @return url 
	 */
	
	public String getUrl() {
		return url;
	}
	/** 
	 * @param url 要设置的 url 
	 */
	
	public void setUrl(String url) {
		this.url = url;
	}
	/** 
	 * @return key 
	 */
	
	public String getKey() {
		return key;
	}
	/** 
	 * @param key 要设置的 key 
	 */
	
	public void setKey(String key) {
		this.key = key;
	}
	/** 
	 * @return remark 
	 */
	
	public String getRemark() {
		return remark;
	}
	/** 
	 * @param remark 要设置的 remark 
	 */
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
}
