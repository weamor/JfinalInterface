package net.weamor.interfaces.pojo.config;

/** 
 * @ClassName Class  
 * @Description 用于存放反射信息的配置文件类 
 *   
 */
public class ClassBean {

	 private String key;
	 private String forName;
	 private String remark;
	 
	 
	/** 
	 * @return key 
	 */
	
	public String getKey() {
		return key;
	}
	/** 
	 * @param key 要设置的 key 
	 */
	
	public void setKey(String key) {
		this.key = key;
	}
	/** 
	 * @return forName 
	 */
	
	public String getForName() {
		return forName;
	}
	/** 
	 * @param forName 要设置的 forName 
	 */
	
	public void setForName(String forName) {
		this.forName = forName;
	}
	/** 
	 * @return remark 
	 */
	
	public String getRemark() {
		return remark;
	}
	/** 
	 * @param remark 要设置的 remark 
	 */
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	 
	 

}
