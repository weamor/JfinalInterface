package net.weamor.interfaces.pojo.config;

import java.util.List;
import java.util.Map;

/** 
 * @ClassName Third  
 * @Description 第三方基本配置信息 
 *   
 */
public class ThirdBean {
	//根据id来进行查找是哪一个项目
	private String id;
	
	//指明项目的调用方式  http REFLECT(反射) 根据此调用方式来决定是用host还是用 class  默认http
	private String type;
	
	//interface层级下的所有key map
	private Map<String , String> configParam;

	//host层级下的key map
	private List<HostBean> hosts;
	
	//class层级下的key map
	private List<ClassBean> classs;
	
	private List<FtpBean> ftps;
	
	
	
	/** 
	 * @return ftps 
	 */
	
	public List<FtpBean> getFtps() {
		return ftps;
	}

	/** 
	 * @param ftps 要设置的 ftps 
	 */
	
	public void setFtps(List<FtpBean> ftps) {
		this.ftps = ftps;
	}

	/** 
	 * @return classs 
	 */
	
	public List<ClassBean> getClasss() {
		return classs;
	}

	/** 
	 * @param classs 要设置的 classs 
	 */
	
	public void setClasss(List<ClassBean> classs) {
		this.classs = classs;
	}

	/** 
	 * @return type 
	 */
	
	public String getType() {
		return type;
	}

	/** 
	 * @param type 要设置的 type 
	 */
	
	public void setType(String type) {
		this.type = type;
	}

	/** 
	 * @return configParam 
	 */
	
	public Map<String, String> getConfigParam() {
		return configParam;
	}

	/** 
	 * @param configParam 要设置的 configParam 
	 */
	
	public void setConfigParam(Map<String, String> configParam) {
		this.configParam = configParam;
	}

	/** 
	 * @return hosts 
	 */
	
	public List<HostBean> getHosts() {
		return hosts;
	}

	/** 
	 * @param hosts 要设置的 hosts 
	 */
	
	public void setHosts(List<HostBean> hosts) {
		this.hosts = hosts;
	}

	/** 
	 * @return id 
	 */
	
	public String getId() {
		return id;
	}

	/** 
	 * @param id 要设置的 id 
	 */
	
	public void setId(String id) {
		this.id = id;
	}

}


