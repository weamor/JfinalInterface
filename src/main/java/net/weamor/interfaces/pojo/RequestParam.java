package net.weamor.interfaces.pojo;

import java.util.Map;

import net.weamor.interfaces.pojo.config.ThirdBean;


/** 
 * @ClassName RequestParam  
 * @Description 请求值处理后的类 
 *   
 */
public class RequestParam extends RequestObject{

	//读取配置文件的url
	private String url;
	//处理后的参数
	private Map<String, Object> argMap;
	
	//再次请求的协议
	private String protocol;
	
	//获得配置文件的信息
	private ThirdBean thirdBean;
	
	//base64 处理方法  amarsoft | def
	private String base64Type;
	
	
	@Override
	public String toString() {
		return "RequestParam [url=" + url + ", argMap=" + argMap
				+ ", protocol=" + protocol + ", thirdBean=" + thirdBean
				+ ", base64Type=" + base64Type + "]";
	}

	public RequestParam(){
		super();
	}

	public String getBase64Type() {
		return base64Type;
	}

	public void setBase64Type(String base64Type) {
		this.base64Type = base64Type;
	}

	public ThirdBean getThirdBean() {
		return thirdBean;
	}

	public void setThirdBean(ThirdBean thirdBean) {
		this.thirdBean = thirdBean;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, Object> getArgMap() {
		return argMap;
	}

	public void setArgMap(Map<String, Object> argMap) {
		this.argMap = argMap;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	
	
}
