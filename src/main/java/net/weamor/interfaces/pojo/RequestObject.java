package net.weamor.interfaces.pojo;


/** 
 * @ClassName Req  
 * @Description 请求基类 
 *   
 */
public class RequestObject {

	/*
	 * 预计格式
	 * {"pro":"LKB",method:"LKB1",caller:"","url":"","param":"[{"paramName":"
	 * aaa","
	 * value":"bb","operate":"DEF"},{"paramName":"aaa","value":"bb","operate
	 * ":"DEF"}]"}
	 */
	// 第三方名称 如果没有第三方项目名称，那么这个时候客户端自行指定
	private String pro;

	// 参数集合
	private String param;
	
	//原始请求的param数据
	private String sparam;

	// 调用者
	private String caller;

	private String method;

	// 请求数据的时候 采用什么编码接收 UTF-8 | GBK...
	private String reqEncoding;

	// head 头信息 Map<String,String> 并且BASE64
	private String head;//

	/*
	 * 参数的类型 XML | JSON | OTHER XML 将直接将此xml数据发送过去，不进行参数化 JSON
	 * 执行内部的json预处理值方式，进行设置name和value方式提交 OTHER 排除XML和JSON
	 * 直接将值进行发送，如果对方直接需要JSON，或者是特殊的格式，那么采用此方式
	 * 
	 * 默认采用Map的方式存储，将去除map的每一个数据进行组装post表单
	 * 
	 * param 	采用param 那么表示有key value  operate
	 * map		表示有key和value
	 * string	表示是一整个串 
	 * 
	 */
	
	private String paramstyle;

	
	// 每次请求的服务器处理唯一序列号
	private String respId;

	// 每次请求调用端携带的序列号
	private String reqId;
	
	@Override
	public String toString() {
		return "RequestObject [pro=" + pro + ", param=" + param + ", sparam="
				+ sparam + ", caller=" + caller + ", method=" + method
				+ ", reqEncoding=" + reqEncoding + ", head=" + head
				+ ", paramstyle=" + paramstyle + ", respId=" + respId
				+ ", reqId=" + reqId + "]";
	}

	public String getSparam() {
		return sparam;
	}

	public void setSparam(String sparam) {
		this.sparam = sparam;
	}
	

	/** 
	 * <p>Title: </p>
	 * <p>Description: </p> 
	 */ 
	
	public RequestObject() {
		super();
	}

	/** 
	 * <p>Title: </p>
	 * <p>Description: </p>
	 * @param pro
	 * @param param
	 * @param caller
	 * @param method
	 * @param reqId 
	 */ 
	
	public RequestObject(java.lang.String pro, java.lang.String param,
			java.lang.String caller, java.lang.String method,
			java.lang.String reqId) {
		super();
		this.pro = pro;
		this.param = param;
		this.caller = caller;
		this.method = method;
		this.reqId = reqId;
	}


	/** 
	 * <p>Title: </p>
	 * <p>Description: </p>
	 * @param pro
	 * @param param
	 * @param sparam
	 * @param caller
	 * @param method
	 * @param reqEncoding
	 * @param head
	 * @param paramstyle
	 * @param respId
	 * @param reqId 
	 */ 
	
	public RequestObject(String pro, String param, String sparam,
			String caller, String method, String reqEncoding, String head,
			String paramstyle, String respId, String reqId) {
		super();
		this.pro = pro;
		this.param = param;
		this.sparam = sparam;
		this.caller = caller;
		this.method = method;
		this.reqEncoding = reqEncoding;
		this.head = head;
		this.paramstyle = paramstyle;
		this.respId = respId;
		this.reqId = reqId;
	}

	public String getRespId() {
		return respId;
	}

	public void setRespId(String respId) {
		this.respId = respId;
	}

	public String getReqEncoding() {
		return reqEncoding;
	}

	public void setReqEncoding(String reqEncoding) {
		this.reqEncoding = reqEncoding;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	/** 
	 * @return head 
	 */

	public String getHead() {
		return head;
	}

	/** 
	 * @param head 要设置的 head 
	 */

	public void setHead(String head) {
		this.head = head;
	}

	/** 
	 * @return paramstyle 
	 */

	public String getParamstyle() {
		return paramstyle;
	}

	/** 
	 * @param paramstyle 要设置的 paramstyle 
	 */

	public void setParamstyle(String paramstyle) {
		this.paramstyle = paramstyle;
	}

	/** 
	 * @return pro 
	 */

	public String getPro() {
		return pro;
	}

	/** 
	 * @param pro 要设置的 pro 
	 */

	public void setPro(String pro) {
		this.pro = pro;
	}

	/** 
	 * @return caller 
	 */

	public String getCaller() {
		return caller;
	}

	/** 
	 * @param caller 要设置的 caller 
	 */

	public void setCaller(String caller) {
		this.caller = caller;
	}

	/** 
	 * @return method 
	 */

	public String getMethod() {
		return method;
	}

	/** 
	 * @param method 要设置的 method 
	 */

	public void setMethod(String method) {
		this.method = method;
	}

}
