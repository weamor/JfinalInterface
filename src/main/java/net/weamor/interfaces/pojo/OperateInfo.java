package net.weamor.interfaces.pojo;

import java.io.Serializable;
import java.util.Date;

public class OperateInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	public Date time;
	public int sort;
	public String msg;

	public OperateInfo(Date time, int sort, String msg) {
		this.time = time;
		this.sort = sort;
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "OperateInfo [time=" + time + ", sort=" + sort + ", msg=" + msg
				+ "]";
	}
}
