package net.weamor.interfaces.pojo;

import java.io.Serializable;

public class Result<T> implements Serializable {
	private static final long serialVersionUID = -8727590546167284227L;
	private T Result;

	public T getResult() {
		return Result;
	}

	@Override
	public String toString() {
		return "Result [Result=" + Result + "]";
	}

}
