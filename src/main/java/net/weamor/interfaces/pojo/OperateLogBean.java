package net.weamor.interfaces.pojo;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import net.weamor.interfaces.utils.date.TimeUtil;


/** 
 * @ClassName OperateLogBean  
 * @Description 日志操作记录类 
 *   
 */
public class OperateLogBean {

	static String host = "";
	static {
		try {
			host = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	/*
	 * 
	 * 这个是用于记录是哪一种操作的key key = apply 表示申请的时候的key key = login 表示登录 key = register
	 * 表示注册 key ....
	 */

	private String key;
	// 操作时间
	private String operateTime;
	// 产生于那一台服务器
	private String server;
	// 操作人，可以使手机，也可以是userId
	private String operate;
	// 操作结果状态
	private String status;
	// 备注说明
	private String remark;

	// 自定义需要记录的数据
	private Map<String, Object> argMap;

	public OperateLogBean(String key, String operateTime, String server,
			String operate, String status, String remark,
			Map<String, Object> argMap) {
		super();
		this.key = key;
		this.operateTime = operateTime;
		this.server = server;
		this.operate = operate;
		this.status = status;
		this.remark = remark;
		this.argMap = argMap;
	}

	public OperateLogBean(String key, String operate, String status,
			String remark, Map<String, Object> argMap) {
		super();
		this.key = key;
		this.operateTime = TimeUtil.getDateNormal();
		this.server = host;
		this.operate = operate;
		this.status = status;
		this.remark = remark;
		this.argMap = argMap;
	}

	public static Map<String, Object> packMap(OperateLogBean operate) {
		Map<String, Object> argMap1 = operate.getArgMap();
		argMap1.put("key", operate.getKey());
		argMap1.put("operateTime", operate.getOperateTime());
		argMap1.put("server", operate.getServer());
		argMap1.put("operate", operate.getOperate());
		argMap1.put("status", operate.getStatus());
		argMap1.put("remark", operate.getRemark());
		return argMap1;
	}

	public String getOperate() {
		return operate;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Map<String, Object> getArgMap() {
		return argMap;
	}

	public void setArgMap(Map<String, Object> argMap) {
		this.argMap = argMap;
	}

}
