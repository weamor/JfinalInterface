package net.weamor.interfaces.pojo;

import java.util.Map;

/** 
 * @ClassName Resp  
 * @Description 返回值基类 
 *   
 */
public class Resp {

	private Map<String, String> data;
	private String code;
	private String msg;

	/** 
	 * @return data 
	 */

	public Map<String, String> getData() {
		return data;
	}

	/** 
	 * @param data 要设置的 data 
	 */

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	/** 
	 * @return code 
	 */

	public String getCode() {
		return code;
	}

	/** 
	 * @param code 要设置的 code 
	 */

	public void setCode(String code) {
		this.code = code;
	}

	/** 
	 * @return msg 
	 */

	public String getMsg() {
		return msg;
	}

	/** 
	 * @param msg 要设置的 msg 
	 */

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
