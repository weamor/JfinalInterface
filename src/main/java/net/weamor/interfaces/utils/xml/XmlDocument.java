package net.weamor.interfaces.utils.xml;

import java.io.InputStream;
import java.util.Map;

import net.weamor.interfaces.pojo.config.ThirdBean;

import org.dom4j.DocumentException;


/** 
 * @ClassName XmlDocument  
 * @Description xml文件解析接口类 
 *   
 */
public interface XmlDocument {
	/**
	 * 
	 * @Title: parserXml 
	 * @Description: 通过文件路径解析XML文件
	 * @param fileName
	 * @return 参数说明
	 * @return Map<Object,Object>    返回类型
	 * @throws
	 */
	public Map<Object,Object> parserXml(String fileName);
	
	/**
	 * 解析多个节点的xml文件
	 * @Title: parserXmlByNode 
	 * @Description: 解析多个节点的xml文件 
	 * @param fileName				文件路径
	 * @return 参数说明
	 * @return Map<String,Third>    返回类型
	 */
	public Map<String, ThirdBean> parserXmlByMutilNode(String fileName) throws DocumentException;
	
	/**
	 * 
	 * @Title: parserXMLStream 
	 * @Description: 通过文件流解析XML文件
	 * @param in
	 * @return 参数说明
	 * @return Map<Object,Object>    返回类型
	 * @throws
	 */
	public Map<Object,Object> parserXMLStream(InputStream in);
	
	/**
	 * 
	 * @Title: getTomcatPath 
	 * @Description: 通过得到Tomcat绝对路径和文件与webapps相对路径得到文件绝对路径
	 * @param path
	 * @return 参数说明
	 * @return String    返回类型
	 * @throws
	 */
	public String getTomcatPath(String path);
	
	
	/**
	 * 获得程序运行时的基础路径
	 * @Title: getRuntimeBasePath 
	 * @Description: 获得程序运行时的基础路径
	 * @return 参数说明
	 * @return String    返回类型
	 */
	public String getRuntimeBasePath();
	
	public String getRuntimeBasePath(String path);
}
