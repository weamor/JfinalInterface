package net.weamor.interfaces.utils.xml;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.weamor.interfaces.pojo.config.ClassBean;
import net.weamor.interfaces.pojo.config.FtpBean;
import net.weamor.interfaces.pojo.config.HostBean;
import net.weamor.interfaces.pojo.config.ThirdBean;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/** 
 * @ClassName Dom4jParserXml  
 * @Description xml文件解析接口实现类  
 *   
 */
public class Dom4jParserXml implements XmlDocument {
	private static Logger gLog = LoggerFactory.getLogger(Dom4jParserXml.class);

	@Override
	@SuppressWarnings("unchecked")
	public Map<Object, Object> parserXml(String fileName) {
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		File inputXml = new File(fileName);
		SAXReader saxReader = new SAXReader();
		try {
			Document document = saxReader.read(inputXml);
			Element employees = document.getRootElement();
			for (Iterator<Object> i = employees.elementIterator(); i.hasNext();) {
				Element employee = (Element) i.next();
				for (Iterator<Object> j = employee.elementIterator(); j
						.hasNext();) {
					Element node = (Element) j.next();
					resultMap.put(node.getName(), node.getText());
					gLog.info("节点名称:" + node.getName() + "  节点内容:"
							+ node.getText());
				}
			}
		} catch (DocumentException e) {
			gLog.error("解析xml文件发送了异常:"+e.getMessage());
		}
		return resultMap;
	}

	
	/**
	 * 解析多个节点的xml文件
	 * @throws DocumentException 
	 */
	public Map<String, ThirdBean> parserXmlByMutilNode(String fileName) throws DocumentException {
		File inputXml = new File(fileName);
		SAXReader saxReader = new SAXReader();
		Map<String, ThirdBean> configMap = new HashMap<String, ThirdBean>();
		Document document = saxReader.read(inputXml);
		List<Element> list1 = document
				.selectNodes("//config/interfaces/interface");
		for (Element n : list1) {
			//接收配置文件中定义的数值
			Map<String, String> configParam = new HashMap<String,String>();
			ThirdBean third = new ThirdBean();
			List<HostBean> hosts = new ArrayList<HostBean>();
			List<ClassBean> classs = new ArrayList<ClassBean>();
			List<FtpBean> ftps = new ArrayList<FtpBean>();
			String id = n.attribute("id").getText();
			if (id.equals("TD")) {
				
				System.out.println("  ");
			}
			
			// 循环得到其子元素
			for (Iterator<Element> i = n.elements().iterator(); i.hasNext();) {
				Element element = (Element) i.next();
				String name = element.getName();
				String text = element.getText();
				
				configParam.put(name, text);

				if ("host".equals(element.getName())) {
					// 循环得到子元素的子元素
					HostBean host = new HostBean();
					for (Iterator<Element> j = element.elements()
							.iterator(); j.hasNext();) {
						Element hostNode = (Element) j.next();

						String hostNodeName = hostNode.getName();
						String hostNodeText = hostNode.getText();
						if ("url".equals(hostNodeName)) {
							host.setUrl(hostNodeText);
						} else if ("key".equals(hostNodeName)) {
							host.setKey(hostNodeText);
						} else if ("remark".equals(hostNodeName)) {
							host.setRemark(hostNodeText);
						}
					}
					hosts.add(host);
				} else if("class".equals(element.getName())){
					ClassBean cla = new ClassBean();
					for (Iterator<Element> j = element.elements()
							.iterator(); j.hasNext();){
						Element hostNode = (Element) j.next();

						String className = hostNode.getName();
						String classText = hostNode.getText();
						if ("forName".equals(className)) {
							cla.setForName(classText);
						} else if ("key".equals(className)) {
							cla.setKey(classText);
						} else if ("remark".equals(className)) {
							cla.setRemark(classText);
						}
					}
					classs.add(cla);
				} else if("ftp".equals(element.getName())){
					FtpBean ftp = new FtpBean();
					for (Iterator<Element> j = element.elements()
							.iterator(); j.hasNext();){
						Element hostNode = (Element) j.next();

						String ftpName = hostNode.getName();
						String ftpText = hostNode.getText();
						if ("ftpHost".equals(ftpName)) {
							ftp.setFtpHost(ftpText);
						} else if ("ftpUserName".equals(ftpName)) {
							ftp.setFtpUserName(ftpText);
						} else if ("ftpUserPasswd".equals(ftpName)) {
							ftp.setFtpUserPasswd(ftpText);
						} else if ("ftpPort".equals(ftpName)){
							ftp.setFtpPort(Integer.parseInt(ftpText));
						} else if ("ftpKey".equals(ftpName)){
							ftp.setFtpKey(ftpText);
						} else if ("ftpType".equals(ftpName)){
							ftp.setFtpType(ftpText);
						}
					}
					ftps.add(ftp);
				}
			}
			third.setConfigParam(configParam);
			third.setId(id);
			third.setHosts(hosts);
			third.setClasss(classs);
			third.setFtps(ftps);
			
			configMap.put(id, third);
		}
		return configMap;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Object, Object> parserXMLStream(InputStream in) {
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		SAXReader saxReader = new SAXReader();
		try {
			Document document = saxReader.read(in);
			Element employees = document.getRootElement();
			for (Iterator<Object> i = employees.elementIterator(); i.hasNext();) {
				Element employee = (Element) i.next();
				for (Iterator<Object> j = employee.elementIterator(); j
						.hasNext();) {
					Element node = (Element) j.next();
					resultMap.put(node.getName(), node.getText());
					System.out.println(node.getName() + ":" + node.getText());
				}
			}
		} catch (DocumentException e) {
			gLog.error(e.getMessage());
		}
		return resultMap;
	}

	@Override
	public String getTomcatPath(String path) {
		String nowPath;
		String tempPath;
		nowPath = System.getProperty("user.dir");
		tempPath = nowPath.replace("bin", "webapps");
		if (tempPath.contains("webapps")) {
			tempPath += "/" + path;
		} else {
			tempPath += "/webapps/" + path;
		}
		return tempPath;
	}

	@Override
	public String getRuntimeBasePath() {
		String nowPath;
		String tempPath;
		nowPath = System.getProperty("user.dir");
		tempPath = nowPath.replace("bin", "fileBackup");
		if (tempPath.contains("fileBackup")) {

		} else {
			tempPath += "/fileBackup";
		}
		return tempPath;
	}

	public String getRuntimeBasePath(String path) {
		String nowPath;
		String tempPath;
		nowPath = System.getProperty("user.dir");
		tempPath = nowPath.replace("bin", "fileBackup");
		if (tempPath.contains("fileBackup")) {
			tempPath += "/" + path + "/";
		} else {
			tempPath += "/fileBackup/" + path + "/";
		}
		return tempPath;
	}

}
