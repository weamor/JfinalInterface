package net.weamor.interfaces.utils;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.weamor.interfaces.global.GlobalVar;
import net.weamor.interfaces.pojo.Param;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/** 
 * @ClassName GsonUtils  
 * @Description JSON转换器 
 *   
 */
public class GsonKit {
	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	/**
	 * 
	 * @Title: retJson 
	 * @Description: 将对象转换为JSON字符串 
	 * @param code
	 * @param msg
	 * @return 参数说明
	 * @return String    返回类型
	 * @throws
	 */
	public static String retJson(String code, String msg) {
		return GsonKit.retJson(code,msg,null);
	}

	/**
	 * 
	 * @Title: retJson 
	 * @Description: 将对象转换为JSON字符串
	 * @param code
	 * @param msg
	 * @param data
	 * @return 参数说明
	 * @return String    返回类型
	 * @throws
	 */
	public static String retJson(String code,String msg,Object data) {
		return GsonKit.retJson(code,msg,data,null);
	}

	/**
	 * 
	 * @Title: retJson 
	 * @Description: 将对象转换为JSON字符串
	 * @param code
	 * @param desc
	 * @param data
	 * @param retain
	 * @return 参数说明
	 * @return String    返回类型
	 * @throws
	 */
	public static String retJson(String code,String desc, Object data, 
			Object retain) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(GlobalVar.CODE, code);
		map.put(GlobalVar.MSG, desc);
		map.put(GlobalVar.DATA, data);
		map.put(GlobalVar.RETAIN, retain);
		return GsonKit.toJson(map);

	}
	
	/**
	 * 
	 * @Title: toJson 
	 * @Description: 将对象转换为JSON字符串
	 * @param obj
	 * @return 参数说明
	 * @return String    返回类型
	 * @throws
	 */
	public static String toJson(Object obj) {
		return gson.toJson(obj);
	}
	
	/**
	 * @Title: getJson 
	 * @Description: 将JSON字符串转换为对象 
	 * @param json
	 * @param classOfT
	 * @return 参数说明
	 * @return T    返回类型
	 * @throws
	 */
	public static <T> T getJson(String json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}
	
	/**
	 * @Title: getJson 
	 * @Description: 将JSON字符串转换为集合
	 * @param json
	 */
	public static <T> List<Param> getJsonToParam(String json) {
		// 用法
//		List<Param> beans = GsonUtils.getJson(json,new TypeToken<List<Param>>() {}.getType());
		return GsonKit.getJson(json,new TypeToken<List<Param>>() {}.getType());
	}
	
	/**
	 * @Title: getJson 
	 * @Description: 将JSON字符串转换为集合
	 * @param json
	 */
	public static <T> List<T> getJson(String json, Type type) {
		// 用法
//		 List<Bean> beans = GsonUtils.getJson(json,new TypeToken<List<Bean>>() {}.getType());
		return gson.fromJson(json, type);
	}
}
