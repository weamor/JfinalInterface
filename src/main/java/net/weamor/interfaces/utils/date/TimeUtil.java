package net.weamor.interfaces.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间类
 * 
 */
public class TimeUtil {
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 返回一个没有加标记分割的时间毫秒结尾 yyyyMMddHHmmssms
	 * */
	public static String getDateNotMarkMS() {
		Date date = new Date();
		return new SimpleDateFormat("yyyyMMddHHmmssms").format(date);
	}

	/**
	 * 显示正常的时间值 yyyy-MM-dd HH:mm:ss
	 * */
	public static String getDateNormal() {
		Date date = new Date();
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	/**
	 * 只显示年月日 yyyy-MM-dd
	 * */
	public static String getDateNoTime() {
		Date date = new Date();
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	/**
	 * 获得当前时间的毫秒数
	 */
	public static long getcurrentTimeMillis() {
		return (long) System.currentTimeMillis();
	}
	
	/**
	 * 获得某一时间的左右范围时间
	 * @param chooseTime 某一时间
	 * @param TimeDis 左右分钟数
	 */
	public static String getRangeTime(String chooseTime,String TimeDis) {
		String chooseTimes = "",bchooseTimes = "";
		if (chooseTime != null) {
			// 使用Time类进行时间的加减
			String date = chooseTime.substring(0, chooseTime.indexOf(" "));
			String times = chooseTime.substring(chooseTime.indexOf(" ") + 1);
			Time time = new Time(times);
			Integer timedis = Integer.parseInt(TimeDis);
			Time t = new Time(0, 0, timedis, 0);
			chooseTimes = (time.addTime(t)).toString();
			
			// 已经将时间进行了增加，现在又要将其进行还原
			chooseTimes = date + " " + chooseTimes;
			chooseTimes = chooseTimes.replace("0, ", "");
			
			// 经过了时间增加后的时间times为"+times+"增加后的时间为:"+chooseTimes
			bchooseTimes = (new Time(times).subtractTime(t)).toString();
			bchooseTimes = date + " " + bchooseTimes;
			bchooseTimes = bchooseTimes.replace("0, ", "");
			
			Date day = null;
			try {
				day = sdf.parse(chooseTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(bchooseTimes.contains(",")){
				bchooseTimes = addDateOneDay(day,-1)+bchooseTimes.substring(bchooseTimes.indexOf(",")+1);
			}
			if(chooseTimes.contains(",")){
				chooseTimes = addDateOneDay(day,1)+chooseTimes.substring(chooseTimes.indexOf(",")+1);
			}
		} 
		return bchooseTimes+"#"+chooseTimes;
	}
	
	/**
	 * 加减日期操作
	 */
	public static String addDateOneDay(Date date,int n) {   
	        Calendar c = Calendar.getInstance();   
	        c.setTime(date);   //设置当前日期  
	        c.add(Calendar.DATE, n); //日期加减   
	        return sdf.format(c.getTime());  
	}  
}
