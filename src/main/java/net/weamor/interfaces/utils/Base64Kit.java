package net.weamor.interfaces.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import it.sauronsoftware.base64.Base64;

/** 
 * @ClassName Base64Coder  
 * @Description Base64工具类 
 *   
 */
public class Base64Kit {
	/**
	 * 
	 * @Title: encodeString 
	 * @Description: 将字符串进行Base64编码
	 * @param str 要转码的字符串
	 * @return String    返回类型
	 * @throws
	 */
	public static String encodeString(String str) {
		return Base64.encode(str, "UTF-8");
	}

	/**
	 * 
	 * @Title: decodeString 
	 * @Description: 将Base64编码后的字符串进行解码 
	 * @param str 要解码的字符串
	 * @return String    返回类型
	 * @throws
	 */
	public static String decodeString(String str) {
		return Base64.decode(str, "UTF-8");
	}

	/*
	 * 图片转化成base64字符串 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
	 */
	public static String getImageStr(String imgFile) {//
		InputStream in = null;
		byte[] data = null;
		// 读取图片字节数组
		try {
			in = new FileInputStream(imgFile);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new String(Base64.encode(data));
	}

	/*
	 * base64字符串转化成图片 对字节数组字符串进行Base64解码并生成图片
	 */
	public static String generateImage(String imgStr,String imgName,String imgPath) {
		// 图像数据为空
		if (imgStr == null) {
			return null;
		}
		try {
			// Base64解码
			byte[] b = Base64.decode(imgStr.getBytes());
			for (int i = 0; i < b.length; ++i) {
				// 调整异常数据
				if (b[i] < 0) {
					b[i] += 256;
				}
			}
			// 生成jpeg图片
			String imgFilePath = imgPath + "/" +imgName;
			OutputStream out = new FileOutputStream(imgFilePath);
			out.write(b);
			out.flush();
			out.close();
			return imgFilePath;
		} catch (Exception e) {
			return null;
		}
	}
}
