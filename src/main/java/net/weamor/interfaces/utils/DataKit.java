package net.weamor.interfaces.utils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/** 
 * @ClassName DataUtil  
 * @Description 数据处理类 
 *   
 */
public class DataKit {

	/**
	 * 获得唯一随机值
	 * @Title: getRandomUnique 
	 * @Description: 获得唯一随机值
	 * @return 参数说明
	 * @return String    返回类型
	 */
	public static String getRandomUnique() {
		StringBuffer str = new StringBuffer();
		long time = System.currentTimeMillis();
		str.append(time);
		Random ran = new Random();
		for (int i = 0; i < 5; i++) {
			str.append(ran.nextInt(9));
		}
		return str.toString();
	}

	public static void main(String[] args) {
		System.out.println(getRandomUnique());
		System.out.println(getRandomUnique().length());
	}
	
	/**
	 * 返回一个没有加标记分割的时间毫秒结尾 yyyyMMddHHmmssms
	 * */
	public static String getDateNotMarkMS() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssms");
		return sdf.format(date);
	}

	/**
	 * 显示正常的时间值 yyyy-MM-dd HH:mm:ss
	 * */
	public static String getDateNormal() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	/**
	 * 只显示年月日 yyyy-MM-dd
	 * */
	public static String getDateNoTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 获得当前时间的毫秒数
	 */
	public static long getcurrentTimeMillis() {
		return (long) System.currentTimeMillis();
	}

	/**
	 * 检查字符串是否为空
	 * @param str 字符串
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (str == null) {
			return true;
		} else if (str.length() == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 检查字符串是否为空
	 * @param str 字符串
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		if (str == null) {
			return false;
		} else if (str.length() == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * String转Date类型
	 * @Title: str2Date 
	 * @Description: String转Date类型
	 * @param str
	 * @return 参数说明
	 * @return Date    返回类型
	 */
	public static Date str2Date(String str) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		// String转Date
		try {
			date = format.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return date;
	}

	/**
	 * String转Date类型
	 * @Title: str2Date 
	 * @Description: String转Date类型
	 * @param str 转换字符串
	 * @param fomart 格式化类型
	 * @return 参数说明
	 * @return Date    返回类型
	 */
	public static Date str2Date(String str,String fomart) {
		DateFormat format = new SimpleDateFormat(fomart);
		Date date = null;
		// String转Date
		try {
			date = format.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return date;
	}
	public static XMLGregorianCalendar long2Gregorian(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		DatatypeFactory dtf = null;
		try {
			dtf = DatatypeFactory.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		XMLGregorianCalendar dateType = dtf.newXMLGregorianCalendar();
		dateType.setYear(cal.get(Calendar.YEAR));
		// 由于Calendar.MONTH取值范围为0~11,需要加1
		dateType.setMonth(cal.get(Calendar.MONTH) + 1);
		dateType.setDay(cal.get(Calendar.DAY_OF_MONTH));
//		dateType.setHour(cal.get(Calendar.HOUR_OF_DAY));
//		dateType.setMinute(cal.get(Calendar.MINUTE));
//		dateType.setSecond(cal.get(Calendar.SECOND));

		return dateType;
	}
	
	/** 
     * 将XMLGregorianCalendar转换为Date 
     * @param cal 
     * @return  
     */  
    public static Date xmlDate2Date(XMLGregorianCalendar cal){  
        return cal.toGregorianCalendar().getTime();  
    } 
    
    /**
   	 * 根据正则表达式验证数据
   	 * @param regx 正则表达式
   	 * @param pObj 待检查对象
   	 * @return boolean 返回的布尔值
   	 */
   	public static boolean isMatch(String regx,String pObj){
   		Pattern pattern = Pattern.compile(regx);
   		Matcher matcher = pattern.matcher(pObj);
   		return matcher.matches();
   	}
   	
   	/**
	 * Double类型验证
	 * @author dingyl
	 * @craeteDae	2014-04-10
	 * @param str	
	 * @return 	true 是 
	 */
	public static boolean valiDouble(String str){
		if (str != null && str.trim().length() != 0) {
			String reg = "^[0-9]*\\.?[0-9]*$";
			Pattern pattern = Pattern.compile(reg);
			Matcher matcher = pattern.matcher(str);
			if (matcher.find()) {
				return true;
			}
		}
		return false;
	}
}
