package net.weamor.interfaces.factory.request;

import java.util.Map;

import net.weamor.interfaces.pojo.RequestParam;


/** 
 * @ClassName RequestFactory  
 * @Description 请求对象工厂类 
 *   
 */
public interface RequestFactory {

	public Map<String, Object> doRequest(RequestParam requestParam,String reqStyle);
}
