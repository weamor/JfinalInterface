package net.weamor.interfaces.factory.param;

import net.weamor.interfaces.pojo.Param;
import net.weamor.interfaces.utils.MD5Kit;


/** 
 * @ClassName Md5Param  
 * @Description  MD5参数操作 
 *   
 */
public class Md5Param implements ParamFactory {


	@Override
	public String operate(Param param, String pro) {
		// TODO Auto-generated method stub
		return MD5Kit.GetMD5Code((String) param.getValue());
	}

}
