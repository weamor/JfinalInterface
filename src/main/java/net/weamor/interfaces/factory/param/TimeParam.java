package net.weamor.interfaces.factory.param;

import net.weamor.interfaces.pojo.Param;



/** 
 * @ClassName TimeParam  
 * @Description TODO 
 *   
 */
public class TimeParam implements ParamFactory {

	@Override
	public String operate(Param param, String pro) {
		long time = System.currentTimeMillis();
		return time + "";
	}

}
