package net.weamor.interfaces.factory.param;

import java.util.Map;

import net.weamor.interfaces.ServiceConfig;
import net.weamor.interfaces.pojo.Param;
import net.weamor.interfaces.pojo.config.ThirdBean;


/** 
 * @ClassName DefaultParam  
 * @Description 默认参数处理 
 *   
 */
public class DefaultParam implements ParamFactory {
	private static Map<String, ThirdBean> configMap = ServiceConfig.configMap;

	@Override
	public String operate(Param param, String pro) {
//		long time = System.currentTimeMillis();
		String paramName = param.getName();
		String paramValue = "";
		// 去configParam 中查找该字段的默认数值

		Map<String, String> configParam = configMap.get(pro)
				.getConfigParam();
		paramValue = configParam.get(paramName);

		return paramValue;
	}

}
