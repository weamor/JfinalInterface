package net.weamor.interfaces.factory.param;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import net.weamor.interfaces.pojo.Param;

/** 
 * @ClassName UrlEncoderParam  
 * @Description URL 参数处理 
 *   
 */
public class UrlEncoderParam implements ParamFactory{

	@Override
	public String operate(Param param, String pro) {
		try {
			return URLEncoder.encode((String) param.getValue(),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
