package net.weamor.interfaces.factory.param;

import net.weamor.interfaces.pojo.Param;



/** 
 * @ClassName ParamFactory  
 * @Description 参数操作工厂类 
 *   
 */
public interface ParamFactory {
	
	/**
	 * 通用的参数处理
	 * @Title: operate 
	 * @Description: 通用的参数处理 
	 * @param param
	 * @param pro
	 * @return 参数说明
	 * @return String    返回类型
	 */
	public String operate(Param param,String pro);
	 

}
